#!/usr/bin/env bash
CONF="/etc/redis.conf";
SENTINEL=""

function add_to_conf(){
    conf=$1
    line=$2
    if grep "$line" $conf > /dev/null; then
        echo "$conf already has line $line"
    else
        echo "$line >> $conf"
        echo $line >> $conf
    fi
}

if [ ! $REDIS_PORT ]; then
    REDIS_PORT="6379"
fi

if [ ! $SENTINEL_PORT ]; then
    SENTINEL_PORT="26379"
fi

if [ $REDIS_BIND ]; then
    add_to_conf $CONF "bind 127.0.0.1 ${REDIS_BIND}"
fi

if [[ $REDIS_TYPE == 'slave' ]]; then
	add_to_conf $CONF "slaveof ${REDIS_MASTER} ${REDIS_PORT}"
elif [[ $REDIS_TYPE == 'sentinel' ]]; then
	CONF="/etc/sentinel.conf"
	SENTINEL="--sentinel"
    add_to_conf $CONF "port ${SENTINEL_PORT}"
    add_to_conf $CONF "sentinel monitor master ${REDIS_MASTER} ${REDIS_PORT} 1"
    add_to_conf $CONF "sentinel down-after-milliseconds master 3000"
    add_to_conf $CONF "sentinel failover-timeout master 6000"
fi

#cat $CONF

# now we run the entrypoint from the redis image
if [ "$1" = "redis" ]; then
    if [[ $REDIS_TYPE == 'sentinel' ]]; then
        exec "/usr/local/bin/docker-entrypoint.sh" "redis-server" ${CONF} ${SENTINEL}
    else
        exec "/usr/local/bin/docker-entrypoint.sh" "redis-server" ${CONF}
    fi
else
    exec "/usr/local/bin/docker-entrypoint.sh" "$@"
fi
